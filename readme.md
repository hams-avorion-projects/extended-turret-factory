# Extended Turret Factory

## Not to be continued. This mod is outdated and is not compatible to the current Modding API!

This mod allows you to modify the seed of turret factories. You can chose the used seed, like you do at the shipyard, to build a turret. This way you can change the stats of built turrets. Therefore I used ressources of the mod Turret Factory Seed Entry by Alopix and version fixes by some others (watch credits below for more information).

For a long time I did not use the Factory Seed Entry mod on our server, because to me it is much overpowered: Build one turret factory and get op weapons. So I did some changes and improved code to balance it.

Also it does some quality of life changes and bugfixes.

## Summary of modifacations
* Modify the factories seed
* Upgrade your factories to gain more seeds
* Added an input field to set the amount of built turrets
* Added a setting to chose the maximum rarity of the turret wich can be built
* Fixed an exploit wich make users able to build Legendary turrets


## Custom seeds
In default settings you still have only the vanilla seed at turret factories, but you can upgrade your own factories to get more available seeds. If you got more then one seed you can chose the seed you want to use for the current weapon, just like you do at a shipyard.

You like the old style, with unlimited amount of seeds? Just change it in the config file.

## Amount of turrets
Choose the amount of turrets you want to build: enter the number and press on Build button one time to build the choosen amount of turrets.

## Configuration
You should take a look in the config file `/mods/ExtTurretFactory/config/ExtTurretFactoryConfig.lua`: you can change a lot of stuff there like the maximum rarity wich can be built, upgrade costs much more.


## Installation instructions
Download the Extended Turret Factory zip file and extract the content of its Avorion directory into your local Avorion directory.

## Compatiblity
This mod overrides the file `/data/scripts/entity/merchants/turretfactory.lua`. I can not provide compatibility to other mods, wich overrides this file.

### Compatiblity to existing galaxies
When you chose seed 0 at your turret factory, it is equal to the vanilla seed. That means (if you did not use the mod "Turret Factory Seed Entry" before) weapons at already existing turret factories will not be changed. But it will add the ability to your own factories to upgrade it to gain more seeds.

If you already used "Turret Factory Seed Entry", it will change all seeds you had before because seeds in this mod includes both: the customized number you can chose and the vanilla seed.

## Credits
This mod uses ressources by some other people:

* Alopix - Creator of this mod
* DevX - Did some compatibility modifications in July '17
* HERC - Modified the mod to work in version 0.15.0
* mechi - Modified the mod to work in version 0.15.7
* douglasg14b - Added the ability to search for best turrets

Many people did changes to this mod, if I missed anybody please let me know, i'll add credits.
