# Changelog
### 0.2.0
* Updated mod to support Avorion 0.21.*

### 0.1.3
* Restored backwards compatibility to a very old version of this mod

### 0.1.2
* Updated mod for Avorion  0.20.2